<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php // variables

$phone = rsd_first_phone();
$email = rsd_first_email();

get_template_part('templates/parts/head'); ?>
<body itemscope itemtype="http://schema.org/WebPage" class="<?php top_class(); ?>">
    <header class="top">
        <div class="wide">
            <div class="top_logo">
                <a href="<?php bloginfo('url'); ?>"><?php echo rsd_logo(); ?></a>
            </div>
            <div class="main_menu">
                <?php wp_nav_menu(['theme_location' => 'primary']); ?>
                <a href="javascript:void(0)" id="mobileNav" class="mobile" onclick="menuToggle()"><i class="fa fa-bars"></i></span></a>
            </div>
        </div>
    </header>
    <nav id="mobileMenu">
        <?php wp_nav_menu(['theme_location' => 'primary']); ?>
    </nav>
    <section id="lightbox">
        <div class="shade"></div>
        <div class="frame">
            <img src="#" alt="">
            <div class="flex">
                <a id="prev" href="javascript:void(0)"><i class="fa fa-chevron-left"></i></a>
                <a id="next" href="javascript:void(0)"><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </section>
