<?php // blog page

get_header();

if (have_posts()) {global $post; ?>
    <section id="theBlogs">
        <div class="slim">
            <?php while (have_posts()) {the_post(); ?>
                <article class="">
                    <h1><?php the_title(); ?></h1>
                    <?php the_excerpt(); ?>
                    <a href="<?php the_permalink(); ?>">Read More</a>
                </article>
            <?php } ?>
        </div>
    </section>
<?php }

get_footer();
