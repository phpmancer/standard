<?php

// most types, menus, etc.
include 'includes/register.php';

// customizer extensions
include 'includes/custom.php';

// shortcut functions
include 'includes/general.php';
