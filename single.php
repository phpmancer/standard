<?php // single post

get_header();

if (have_posts()) {global $post;
    while (have_posts()) {the_post();

        get_template_part('templates/parts/banner');
        get_template_part('templates/parts/content');

    }
}

get_footer();
