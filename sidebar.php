<?php // sidebar

global $post;
$post_type = get_post_type($post);

if (!empty($post->post_parent)) {
    $parent = get_post($post->post_parent);
    $ancestors = get_post_ancestors($post->ID);
    $root=count($ancestors)-1;
    $parent = $ancestors[$root];
} else {$parent = $post->ID;}

$children = wp_list_pages([
    'title_li' => '',
    'child_of' => $parent,
    'echo' => 0,
    'post_type' => $post_type,
    'sort_column' => 'menu_order, post_name',
]);

$dropdown = wp_dropdown_pages([
    'child_of' => $parent,
    'echo' => 0,
    'show_option_none' => 'Select...',
    'sort_column' => 'menu_order'
]);

if ($dropdown) {
    $submenu = str_replace('<select ', '<select onchange="this.form.submit()" ', $dropdown);
} else {$submenu = '';}

if(!empty($children)) { ?>
    <nav class="sidebar">
        <div class="sidebar_header">
            <a href="<?php the_permalink($parent); ?>" class="trigger"><?php the_title($parent); ?></a>
        </div>
        <ul><?php echo $children; ?></ul>
        <form action="<?php echo site_url('/'); ?>" class="mobile_sidebar" method="get">
            <?php echo $submenu ?>
        </form>
    </nav>
<?php }
