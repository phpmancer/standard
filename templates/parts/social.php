<?php // social links

$social_links = [];

$facebook = get_theme_mod('facebook');
if (!empty($facebook)) {
    $social_links[] = [
        'network' => 'facebook',
        'link' => $facebook,
    ];
}

$twitter = get_theme_mod('twitter');
if (!empty($twitter)) {
    $social_links[] = [
        'network' => 'twitter',
        'link' => $twitter,
    ];
}

$instagram = get_theme_mod('instagram');
if (!empty($instagram)) {
    $social_links[] = [
        'network' => 'instagram',
        'link' => $instagram,
    ];
}

$youtube = get_theme_mod('youtube');
if (!empty($youtube)) {
    $social_links[] = [
        'network' => 'youtube',
        'link' => $youtube,
    ];
}

$linkedin = get_theme_mod('linkedin');
if (!empty($linkedin)) {
    $social_links[] = [
        'network' => 'linkedin',
        'link' => $linkedin,
    ];
}

$pinterest = get_theme_mod('pinterest');
if (!empty($pinterest)) {
    $social_links[] = [
        'network' => 'pinterest',
        'link' => $pinterest,
    ];
}

if(!empty($social_links)) { ?>
    <div class="social_links">
        <?php foreach($social_links as $social) { ?>
            <a href="<?php echo $social['link'] ?>" target="_blank">
    			<i class="fab fa-<?php echo $social['network'] ?>"></i>
            </a>
        <?php } ?>
    </div>
<?php }
