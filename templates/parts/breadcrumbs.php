<?php // breadcrumbs

$bcLinks = [];
$parent = $post->post_parent;

if ($parent) {
    while ($parent) {
        array_unshift($bcLinks, ['name' => get_the_title($parent),'link' => get_the_permalink($parent)]);
        $parent = wp_get_post_parent_id($parent);
    }
}

array_unshift($bcLinks, ['name' => 'Home','link' => get_bloginfo('url')]);

$bcLinks[] = ['name' => get_the_title(),'link' => get_the_permalink()];

if ($bcLinks) { ?>
    <ul class="inline" id="breadcrumbs">
        <?php foreach ($bcLinks as $l): ?>
            <li><a href="<?php echo $l['link'] ?>"><?php echo $l['name'] ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php } ?>
