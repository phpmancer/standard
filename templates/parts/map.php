<div class="mapApi">
    <div id="theMap"></div>
</div>
<?php rsd_map_script();

function rsd_map_script() { ?>
    <script type="text/javascript">
        function initmap() {
            var locations = [<?php rsd_map_locations(); ?>];
            var map = new google.maps.Map(document.getElementById('theMap'), {
                zoom: 13,
                scrollwheel: false,
                center: new google.maps.LatLng(35.5797469,-77.0903568),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                // styles:
            });

            var infowindow = new google.maps.InfoWindow({});
            var marker, i;
            var image = new google.maps.MarkerImage('<?php echo get_template_directory_uri(); ?>/css/img/mapicon.png', null, null, null, new google.maps.Size(62,62));
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    // icon: image
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                        position: new google.maps.LatLng(map.lat, map.lng);
                    }
                })(marker, i));
            }
        }
    </script>
    <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyDkpIOq7XZJ9wbHaLQLY0mMmNkT-qIwzg0&libraries=places,geometry&callback=initmap" async defer></script>
    <?php
}

function rsd_map_locations() {$locationList = '';

    $company_name = get_bloginfo('name');
    // for single location
    $address = get_field('company_address', 'option');
    $coordinates = get_field('coordinates', 'option');
    $map = explode(',', $coordinates);
    $lat = $map[0];
    $lon = $map[1];
    // for multiple locations

    $address = str_replace(PHP_EOL, '', $address);
    $address = str_replace(array("\n","\r"), '', $address);
    $address_gmaps = str_replace('<br />', ',', $address);
    $address_gmaps = str_replace(' ', '+', $address_gmaps);

    $locationList .= '[\'<div class="map_info_window"><h3>'.$company_name.'</h3>'.$address.'<a href="//www.google.com/maps/dir/'.$address_gmaps.'//" class="btn pbgh" target="_blank" title="Click to view directions">Directions</a></div>\', \''.$lat.'\', \''.$lon.'\', 1],';
    echo $locationList;
}
