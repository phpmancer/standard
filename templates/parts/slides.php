<?php // slides section

$slides = get_field('slides');

if ($slides) { ?>
    <section id="slides">
        <?php foreach ($slides as $s): ?>
            <figure style="background-image: url(<?php echo $s['image'] ?>)">
                <div class="slideContent">
                    <div class="wrapper">
                        <h2 class="loudest"><?php echo $s['title'] ?></h2>
                        <?php if ($s['subtext']): ?>
                            <h3 class="quiet"><?php echo $s['subtext'] ?></h3>
                        <?php endif; ?>
                        <?php rsd_links($s['links'], 'btn'); ?>
                    </div>
                </div>
            </figure>
        <?php endforeach; ?>
    </section>
<?php }
