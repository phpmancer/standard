<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <link rel="shortcut icon" href="<?php echo rsd_favicon(); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <?php // get site colors
    $primary_color = get_theme_mod('primary_color');
    $primary_hover = get_theme_mod('primary_hover');

    $secondary_color = get_theme_mod('secondary_color');
    $secondary_hover = get_theme_mod('secondary_hover');

    $tertiary_color = get_theme_mod('tertiary_color');
    $tertiary_hover = get_theme_mod('tertiary_hover');

    // convert to RGB
    $pm_hex = str_replace('#', '', $primary_color);
    $r = hexdec(substr($pm_hex, 0, 2));
    $g = hexdec(substr($pm_hex, 2, 2));
    $b = hexdec(substr($pm_hex, 4, 2));
    $primary_rgba = "rgba($r, $g, $b, .9);" ?>
    <style media="screen">

        /* Generic Color Classes */
        .pbg, .pbgh {background: <?php echo $primary_color ?>;}
        .pbgh:hover {background: <?php echo $primary_hover ?>;}

        .sbg, .sbgh {background: <?php echo $secondary_color ?>;}
        .sbgh:hover {background: <?php echo $secondary_hover ?>;}

        .tbg, .tbgh {background: <?php echo $tertiary_color ?>;}
        .tbgh:hover {background: <?php echo $tertiary_hover ?>;}

        .pfc, .pfch, .entry a {color: <?php echo $primary_color ?>;}
        .pfch:hover, .entry a:hover {color: <?php echo $primary_hover ?>;}

        .sfc, .sfch {color: <?php echo $secondary_color ?>;}
        .sfch:hover {color: <?php echo $secondary_hover ?>;}

        .tfc, .tfch {color: <?php echo $tertiary_color ?>;}
        .tfch:hover {color: <?php echo $tertiary_hover ?>;}

        /*Extra Stuff*/
        .entry blockquote {border-left: 4px solid <?php echo $primary_color ?>;}
        #mobileMenu .menu li[class*="current"] > a,
        #mobileMenu .menu li a:hover {background: <?php echo $primary_color ?>; color: #fff;}
    </style>
<?php wp_head(); ?>
<?php $analytics = get_field('analytics', 'option');
if ($analytics): ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $analytics ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '<?php echo $analytics ?>');
    </script>
<?php endif; ?>
</head>
