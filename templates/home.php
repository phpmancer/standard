<?php // Template Name: Home Page

get_header();

if (have_posts()) {global $post;
    while (have_posts()) {the_post();

        get_template_part('templates/parts/slides');

    }
}

get_footer();
