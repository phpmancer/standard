(function() {
    tinymce.create('tinymce.plugins.MyButtons', {
        init : function(ed, url) {
            ed.addButton('rsd_mce_cta', {text : 'BTN',
                onclick : function() {
                    var user_url = prompt('Enter URL here', 'https://');
                    if(user_url != null) {ed.selection.setContent('<a target="_blank" class="btn pbgh" href="'+user_url+'">'+ed.selection.getContent()+'</a>');}
                } // on click
            });
        }, createControl : function(n, cm) {return null;},
 }); tinymce.PluginManager.add('rsd_mce_buttons', tinymce.plugins.MyButtons );
})();
