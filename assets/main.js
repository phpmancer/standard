// with jQuery

jQuery(window).resize(function(){ theResizer(); });

function theResizer() {
    // add resizing js here

}

jQuery(window).scroll(function() {
    var wScroll = jQuery(window).scrollTop();
    var wHeight = jQuery(window).height();
    var wBottom = wScroll + wHeight;
    var winFrac = wHeight / 5;
    // scroll functions
});

jQuery(document).ready(function(){

    jQuery('#slides').slick({
        fade: true,
        // arrows: true
    });

    // lightbox functionality
    var galleryTotal = 0;
    var galleryCount = 0;
    jQuery('ul.gallery li a').each(function() {galleryTotal++;
        jQuery(this).attr('id', galleryTotal);
    }).on('click', function(e) {
        jQuery('#lightbox').addClass('on');
        var url = jQuery(this).attr("href");
        jQuery('#lightbox img').attr('src', url);
        galleryCount = jQuery(this).attr('id');
        alert(galleryCount);
        e.preventDefault();
        jQuery('#lightbox p').text(galleryCount+' / '+galleryTotal);
    });
    jQuery('#lightbox a').on('click', function(e) {
        var actionID = jQuery(this).attr('id');
        if (actionID == "prev") {
            galleryCount--;
            if (galleryCount <= 0) {galleryCount = galleryTotal;}
            var url = jQuery('ul.gallery li a#'+galleryCount).attr("href");
            jQuery('#lightbox img').attr('src', url);
        } else if (actionID == "next") {
            galleryCount++;
            if (galleryCount > galleryTotal) {galleryCount = 1;}
            var url = jQuery('ul.gallery li a#'+galleryCount).attr("href");
            jQuery('#lightbox img').attr('src', url);
        } else {
            jQuery('#lightbox').removeClass('on');
        } e.preventDefault();
    });
    jQuery('#lightbox').on('click', function() {
        if (!jQuery('#lightbox .frame').is(':hover')) {
            jQuery('#lightbox').removeClass('on');
        } else {}
    });

    // run resizing functions
    theResizer();
    setTimeout(function(){theResizer();}, 500);

});

// menu toggle
function menuToggle(e) {
    jQuery('#mobileMenu').toggleClass("on");
    e.preventDefault();
}

// make elements of a type match height
function rsdHeightBox(resize) {
    var maxBlockHeight = 0;
    jQuery(resize).css({'height' : 'auto'});
    jQuery(resize).each(function() {
        if (jQuery(this).outerHeight(false) > maxBlockHeight) {
            maxBlockHeight = jQuery(this).outerHeight(false);
        } else {}
    });
    jQuery(resize).css({'height': maxBlockHeight});
}

function rsdElementHeight(element1, element2 ='', number = '') {
    if (element2.length == 0) {element2 = element1;}
    if (number.lenght == 0) {number = 1;}
    var theHeight = jQuery(element1).width() * number;
    jQuery(element2).css({'height': theHeight});
}



// reveal function
jQuery.fn.reveal = function() {
    return this.each(function() {
        var objectOffset = jQuery(this).offset();
        var objectOffsetTop = objectOffset.top + winFrac;
        if (!jQuery(this).hasClass('animate')) {
            if (wBottom > objectOffsetTop) {jQuery(this).addClass('animate');}
        } else {
            if (wBottom <= objectOffsetTop) {jQuery(this).removeClass('animate');}
        }
    });
}
