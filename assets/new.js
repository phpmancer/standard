// without jQuery

window.addEventListener('resize', allResizing, false);
setTimeout(function(){allResizing();}, 250);

function allResizing() {
    // all resizing goes here
} allResizing();

window.onscroll = function() {

} // scrolling


// mobile menu
var mobileNav = document.querySelector('#mobileNav');
var mobileMenu = document.querySelector('#mobileMenu');
if (mobileNav && mobileMenu) {
    mobileNav.onclick = function(event) {
        mobileMenu.classList.toggle('on');
        event.preventDefault();
    } // click
} // mobileNav


// lightbox/gallery
var gallery = document.querySelectorAll('.gallery a');
var lbox = document.querySelector('#lightbox');
if (gallery && lbox) {
    var galleryTotal = 0;
    var currentImg = 0;
    var lbimg = document.querySelector('#lightbox img');
    for (var i = 0; i < gallery.length; i++) { galleryTotal++;
        gallery[i].setAttribute('count', galleryTotal);
        gallery[i].onclick = function(event) {
            var theImage = this.getAttribute('href');
            currentImg = this.getAttribute('count');
            lbimg.setAttribute('src', theImage);
            lbox.classList.add('on');
            event.preventDefault();
        } // add event
    } // each
    document.querySelector('#lightbox .shade').onclick = function() {
        lbox.classList.remove('on');
    } // close
    document.querySelector('#lightbox a#prev').onclick = function() {currentImg--;
        if (currentImg <= 0) {currentImg = gallery.length;}
        var newImage = document.querySelector('[count="'+currentImg+'"]');
        lbimg.setAttribute('src', newImage.getAttribute('href'));
    } // prev
    document.querySelector('#lightbox a#next').onclick = function() {currentImg++;
        if (currentImg >= gallery.length) {currentImg = 1;}
        var newImage = document.querySelector('[count="'+currentImg+'"]');
        lbimg.setAttribute('src', newImage.getAttribute('href'));
    } // next
} // gallery


//
function rsdHeightBox(target) { maxHeight = 0;
    var elements = document.querySelectorAll(target);
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.height = "auto";
    } // reset
    for (var i = 0; i < elements.length; i++) {
        var iHeight = elements[i].offsetHeight;
        if (iHeight >= maxHeight) {maxHeight = iHeight;}
    } // find height
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.height = maxHeight+"px";
    } // set height
} // rsdHeightBox



function rsdWidthBox(element1, element2='', number='') {
    var first = document.querySelectorAll(element1);
    if (element2.length == 0) {second = first;}
    else {second = document.querySelectorAll(element2);}
    if (number == false) {number = 1;}
    theHeight = first[0].offsetWidth * number;
    for (var i = 0; i < second.length; i++) {
        second[i].style.height = theHeight+"px";
    } // set height
} // rsdElementHeight



function slider(params) {
    let slider = document.querySelector(params.parent);
    if (slider) { slider.classList.add('slider');
        let slides = document.querySelectorAll(params.parent+' > *');
        let current = 0;
        slides[current].classList.add('active');
        setInterval(function() {
            slides[current].classList.remove('active');
            current++;
            if (current >= slides.length) {current = 0;}
            slides[current].classList.add('active');
        }, params.interval);
    }
}
