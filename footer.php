<footer>
    <div class="wide">
        <div class="footerLogo"><a href="<?php bloginfo('url') ?>"><?php echo rsd_logo(); ?></a></div>
        <div class="copyright">
            <span><?php echo '&copy;'.date('Y').' '.get_bloginfo('name'); ?></span> <span>Digital Marketing and Web Design by</span> <span><a href="https://redsharkdigital.com" target="_black">Red Shark Digital</a></span>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
