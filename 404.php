<?php get_header(); ?>
<section class="internal">
    <div class="container">
        <h1>Error</h1>
        <div class="content entry">
            <h2>Sorry, but the page you requested does not exist.</h2>
        </div>
    </div>
</section>
<?php get_footer();
