<?php // customizer

add_action('customize_register', 'rsd_theme_customizer');
function rsd_theme_customizer($wp_customize) {

    // add options to "Site Identity"
    $wp_customize->add_setting('site_logo', []);
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'site_logo', [
        'label' => 'Site Logo',
        'section' => 'title_tagline',
        'settings' => 'site_logo'
    ]));

    // color support
    $wp_customize->add_section('rsd_color_options', [
        'title' => 'Site Colors',
        'priority' => 1
    ]);

    // set colors in array for loop
    $colors = [
        ['name' => 'Primary Color', 'default' => '#111111'],
        ['name' => 'Primary Hover', 'default' => '#444444'],
        ['name' => 'Secondary Color', 'default' => '#222222'],
        ['name' => 'Secondary Hover', 'default' => '#555555'],
        ['name' => 'Tertiary Color', 'default' => '#333333'],
        ['name' => 'Tertiary Hover', 'default' => '#666666'],
    ];

    // Add All Color Options
    foreach ($colors as $c) {

        // format color names
        $safeName = str_replace(' ', '_', $c['name']);
        $safeName = strtolower($safeName);

        $wp_customize->add_setting($safeName, ['default' => $c['default']]);
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, $safeName, [
            'label' => __($c['name'], 'rsd'),
            'section' => 'rsd_color_options',
            'settings' => $safeName,
        ]));
    }

    // social links
    $wp_customize->add_section('rsd_social_links', [
        'title' => 'Social Links',
        'priority' => 2
    ]);

    // social link array
    $social_links = [
        ['platform' => 'Facebook'],
        ['platform' => 'Twitter'],
        ['platform' => 'Instagram'],
        ['platform' => 'YouTube'],
        ['platform' => 'LinkedIn'],
        ['platform' => 'Pinterest'],
    ];

    foreach ($social_links as $link) {
        $safeName = strtolower($link['platform']);
        $wp_customize->add_setting($safeName, []);
        $wp_customize->add_control(new WP_Customize_Control($wp_customize, $safeName, [
            'label' => __($link['platform'], 'rsd'),
            'section' => 'rsd_social_links',
            'settings' => $safeName
        ]));
    }

}
