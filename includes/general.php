<?php

// place span tags inside string
function rsd_span($str = '') {
    $str = preg_replace('#\*{1}(.*?)\*{1}#', '<span>$1</span>', $str);
    return $str;
}


// format phone number
function rsd_phone($phone) {
    $phone = preg_replace('/[^0-9]/', '', $phone);
	if(strlen($phone) == 7) {echo 'tel:'.preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $phone);}
    elseif (strlen($phone) == 10) {echo 'tel:+1-'.preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1-$2-$3', $phone);}
    else {echo 'tel:'.$phone;}
}


// loop through links, classes optional
function rsd_links($links, $classes = '') {
    if ($links) { ?>
        <div class="links">
            <?php foreach ($links as $l): ?>
                <a href="<?php echo $l['path'] ?>" class="<?php echo $classes ?>"><?php echo $l['name'] ?></a>
            <?php endforeach; ?>
        </div>
    <?php }
}


// get featured image as background
function rsd_image($image = '') {
    if (empty($image)) {$image = get_the_post_thumbnail_url();}
    if ($image) {$bg = "background-image: url($image);";}
    else {$bg = '';} echo $bg;
}


function rsd_short($count = 100) {
    $excerpt = strip_tags(get_the_excerpt());
    $excerpt = substr($excerpt, 0, $count);
    echo $excerpt."[...]";
}


// get the first phone
function rsd_first_phone() {
    $phones = get_field('phone_list', 'option');
    if ($phones) {$phone = $phones[0]['number'];}
    else{$phone = '';}
    return $phone;
}


// get the first email
function rsd_first_email() {
    $emails = get_field('email_list', 'option');
    if ($emails) {$email = $emails[0]['email'];}
    else{$email = '';}
    return $email;
}


// determine logo
function rsd_logo() {
    $logo = get_theme_mod('site_logo');
    if ($logo) {$logoLink = '<img src="'.$logo.'" alt="">';}
    else {$logoLink = get_bloginfo('name');}
    return $logoLink;
}


// get favicon
function rsd_favicon() {
    $favicon = get_theme_mod('site_icon');
    if (!$favicon) {
        $favicon = get_theme_mod('site_logo');
    }
    return $favicon;
}


// body classes
function top_class() {
    $template = str_replace('.php','', get_page_template_slug());
    $template = str_replace('templates/','', $template);
    if (empty($template)) {$template = "standard";}
    $posttype = get_post_type();
    echo "$template $posttype";
}
