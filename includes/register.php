<?php // register

// TinyMCE Additions
function rsd_tinymce_styles() {add_editor_style('editor-style.css');}
add_action('admin_init', 'rsd_tinymce_styles');
function rsd_add_tinymce_plugin($plugin_array) {
    $plugin_array['rsd_mce_buttons'] = get_template_directory_uri() .'/assets/tinymce.js';
    return $plugin_array;
}
function rsd_register_mce_button($buttons) {
    array_push($buttons,'rsd_mce_cta');
    return $buttons;
} add_action('admin_head', 'rsd_add_mce_button');
function rsd_add_mce_button() {
    if(!current_user_can('edit_posts') && !current_user_can('edit_pages')) {return;}
    add_filter('mce_external_plugins', 'rsd_add_tinymce_plugin');
    add_filter('mce_buttons', 'rsd_register_mce_button');
}

// reorder the menu
function rsd_admin_menu_order($menu_order) {
    if ($menu_order) {
        return ['index.php', 'edit.php?post_type=page', 'edit.php',];
    } else {return true;}
}
add_filter('custom_menu_order', 'rsd_admin_menu_order');
add_filter('menu_order', 'rsd_admin_menu_order');

// all initializations
function rsd_initialize() {
    register_nav_menus([
        'primary' => __('Primary Menu'),
    ]);

    register_taxonomy('content_type', 'page', ['label' => 'Content Type']);

    // custom posts
    // register_post_type ('location', [
    //     'labels' => ['name' => __('Locations'), 'singular_name' => __('Location')],
    //     'supports' => ['thumbnail', 'editor', 'excerpt', 'title', 'revisions', 'page-attributes'],
    //     'rewrite' => ['slug' => 'locations'],
    //     'has_archive' => true,
    //     'public' => true,
    // ]); // locations
} add_action('init', 'rsd_initialize');

// Adding Theme Support
add_theme_support('post-thumbnails');
add_theme_support('title-tag');

// main options post type
if(function_exists('acf_add_options_page')) {
    $option_page = acf_add_options_page([
        'page_title' => 'Site Globals',
        'menu_title' => 'Site Globals',
        'menu_slug' => 'site-globals',
        'capability' => 'edit_posts',
        'position' => '0.125',
        'redirect' => true,
    ]);
} else {}

// register styles/scripts
define('RSD_V', '2.1');
function rsd_scripts() {
    wp_register_style('style_css', get_template_directory_uri().'/style.css', '', RSD_V);
    wp_register_style('admin_css', get_template_directory_uri().'/assets/admin.css', '', RSD_V);

    wp_register_script('main_scripts', get_template_directory_uri().'/assets/main.js', '', RSD_V, true);
    wp_register_script('plugin_scripts', get_template_directory_uri().'/assets/plugins.js', '', RSD_V, true);

    if(!is_admin()) {
        wp_enqueue_style('style_css');
        wp_enqueue_script('jquery-core');
        wp_enqueue_script('plugin_scripts');
        wp_enqueue_script('main_scripts');
    } else {}
} add_action('wp_enqueue_scripts', 'rsd_scripts');

function admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/assets/admin.css');
} add_action('admin_enqueue_scripts', 'admin_style');
